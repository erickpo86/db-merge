from db_connection import get_connection_to
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import Integer
from sqlalchemy import SmallInteger
from sqlalchemy import String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Funcionario(Base):
	__tablename__ = 'funcionario'

	func_id				= Column(Integer, primary_key=True)
	matricula			= Column(Integer)
	nome				= Column(String)
	dtnasc				= Column(Date)
	mae					= Column(String)
	pai					= Column(String)
	cpf					= Column(String)
	numrg				= Column(String)
	orgaorg				= Column(String)
	ufrg				= Column(String)
	expedrg				= Column(Date)
	sit_recag			= Column(String)
	orgao				= Column(String)
	cod_barras			= Column(String)
	st_id				= Column(Integer)
	name				= Column(String)
	words				= Column(Integer)
	num_vinculos		= Column(SmallInteger)
	homonimo_status		= Column(Integer)
	estado_civil		= Column(String)
	sexo				= Column(String)

	def __init__(self, matricula, nome, dtnasc, mae, pai, cpf, numrg, orgaorg,
				 ufrg, expedrg, sit_recag, orgao, cod_barras, st_id, name,
				 words, num_vinculos, homonimo_status, estado_civil, sexo, func_id):
		self.matricula			= matricula
		self.nome				= nome
		self.dtnasc				= dtnasc
		self.mae				= mae
		self.pai				= pai
		self.cpf				= cpf
		self.numrg				= numrg
		self.orgaorg			= orgaorg
		self.ufrg				= ufrg
		self.expedrg			= expedrg
		self.sit_recag			= sit_recag
		self.orgao				= orgao
		self.cod_barras			= cod_barras
		self.st_id				= st_id
		self.name				= name
		self.words				= words
		self.num_vinculos		= num_vinculos
		self.homonimo_status	= homonimo_status
		self.estado_civil		= estado_civil
		self.sexo				= sexo
		self.func_id			= func_id