# -*- coding: utf-8 -*-
from db_connection import get_connection_to
from cargo	import Cargo
from sqlalchemy.sql import exists
import sys
import logging

def importar_cargos(database_name):

	source, destination = get_connection_to(database_name)

	cargos = source.query(Cargo).all()

	total_records = source.query(Cargo).count()

	logging.basicConfig(filename='cargo.log',level=logging.DEBUG)

	print '\nProcessando '+database_name + ' para Cargos ['+str(total_records)+']'
	logging.info('Processando '+database_name + ' para Cargos ['+str(total_records)+']'
	)

	position = 0
	inserts = 0
	rejecteds = 0

	for cargo in cargos:
		""" Importa todos os cargos verificando se existe um cargo com todos os
		dados iguais"""
		existe_cargo = destination.query(exists().where(Cargo.nome == cargo.nome).
		where(Cargo.extincao == cargo.extincao).
		where(Cargo.categoria == cargo.categoria).
		where(Cargo.subcategoria == cargo.subcategoria).
		where(Cargo.adicionado == cargo.adicionado)).scalar()

		if existe_cargo == False:
			destination.add(Cargo(cargo.nome, cargo.extincao, cargo.categoria,
							cargo.subcategoria, cargo.adicionado))
			destination.commit()
			inserts += 1
		else:
#			logging.info('['+database_name+'] Ja existe apostilamento para func_id: '+str(apostilamento.func_id))+' - ignorando registro.'
			rejecteds += 1

		percent = ( ( position * 100 ) / total_records )
		position += 1
		print str(percent) + '%'
		sys.stdout.write("\033[F")

	print "Inseridos: "+str(inserts)
	logging.info('['+database_name+'] Inseridos: '+str(inserts))
	print "Rejeitados: "+str(rejecteds)
	logging.info('['+database_name+'] Rejeitados: '+str(rejecteds))

