# -*- coding: utf-8 -*-
from db_connection import get_connection_to
from delimitador_evento import DelimitadorEvento
from sqlalchemy.sql import exists
import sys
import logging

def importar_delimitador_evento(database_name):

	source, destination = get_connection_to(database_name)

	delimitadores = source.query(DelimitadorEvento).all()

	total_records = source.query(DelimitadorEvento).count()

	logging.basicConfig(filename='delimitador_evento.log',level=logging.DEBUG)

	print '\nProcessando '+database_name + ' para Delimitador Evento ['+str(total_records)+']'
	logging.info('Processando '+database_name + ' para Delimitador Evento ['+str(total_records)+']'
	)

	position = 0
	inserts = 0
	rejecteds = 0

	for delimitador in delimitadores:
		""" Importa todos os cargos verificando se existe um cargo com todos os
		dados iguais"""
		existe_delimitador = destination.query(exists().
		where(DelimitadorEvento.del_nome == delimitador.del_nome).
		where(DelimitadorEvento.del_palavra == delimitador.del_palavra).
		where(DelimitadorEvento.del_tipo == delimitador.del_tipo).
		where(DelimitadorEvento.del_hierarquia == delimitador.del_hierarquia).
		where(DelimitadorEvento.ativo == delimitador.ativo)).scalar()

		if existe_delimitador == False:
			destination.add(DelimitadorEvento(delimitador.del_nome, delimitador.del_palavra,
							delimitador.del_tipo, delimitador.del_hierarquia,delimitador.ativo))
			destination.commit()
			inserts += 1
		else:
#			logging.info('['+database_name+'] Ja existe apostilamento para func_id: '+str(apostilamento.func_id))+' - ignorando registro.'
			rejecteds += 1

		percent = ( ( position * 100 ) / total_records )
		position += 1
		print str(percent) + '%'
		sys.stdout.write("\033[F")

	print "Inseridos: "+str(inserts)
	logging.info('['+database_name+'] Inseridos: '+str(inserts))
	print "Rejeitados: "+str(rejecteds)
	logging.info('['+database_name+'] Rejeitados: '+str(rejecteds))

