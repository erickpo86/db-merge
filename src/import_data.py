# -*- coding: iso-8859-15 -*-
from db_connection import get_connection_to
from apostilamento import Apostilamento
from funcionario import Funcionario
from sqlalchemy.sql import exists
import sys

def execute(database_name):
	
	source, destination = get_connection_to(database_name)

	funcionarios = source.query(Funcionario).all()

	total_records = source.query(Funcionario).count()

	print '\nProcessing '+database_name + '['+str(total_records)+']'

	position = 0

	max_func_id = 0
	inserts = 0
	rejecteds = 0
	func_id_changed = 0

	for func in funcionarios:
		if func.func_id > max_func_id:
			max_func_id = func.func_id

		# nao pode ter funcionario com a mesma matricula

		total_func_id = destination.query(Funcionario).filter_by(func_id = func.func_id).count()
		total_matricula = destination.query(Funcionario).filter_by(matricula = func.matricula).count()
		if total_func_id == 0:
			# nao existe para este func_id, verificar a matricula e o orgao.
			if total_matricula == 0:
				# nao existe para esta matricula
				destination.add(Funcionario(func.matricula, func.nome,func.dtnasc,
				func.mae, func.pai, func.cpf, func.numrg, func.orgaorg, func.ufrg,
				func.expedrg, func.sit_recag, func.orgao, func.cod_barras, func.st_id,
				func.name, func.words, func.num_vinculos, func.homonimo_status,
				func.estado_civil, func.sexo, func.func_id))
				destination.commit()
				inserts += 1
			else:
				# existe para esta matricula mas nao com o mesmo func_id
				if total_matricula == 1:
					registro_novo = destination.query(Funcionario).filter_by(matricula = func.matricula).first()
					if registro_novo.nome == func.nome:
						#nome igual, matricula igual mas func_id diferente
						print '[indexer_'+database_name+'] Nome igual, matricula igual mas func_id diferente!'
						print '[indexer_'+database_name+'] Nome: '+str(func.nome)+' ,Matricula: '+str(func.matricula)+' ,func_id legado: '+str(func.func_id)+' , func_id novo: '+str(registro_novo.func_id)
						rejecteds += 1

					else:
						# nome e func_id diferentes mas matricula igual
						print '[indexer_'+database_name+'] Nome e func_id diferentes mas mesma matricula!'
						print '[indexer_'+database_name+'] Matricula: '+str(func.matricula)
						print '[indexer_'+database_name+'] Nome: '+str(func.nome)+' , Func_id: '+str(func.func_id)
						print '[novo] Nome: '+str(registro_novo.nome)+' , Func_id: '+str(registro_novo.func_id)
						rejecteds += 1
						
		else:
			# existe este func_id ja gravado no banco novo
			if total_matricula == 0:
				total_para_nome_e_func_id = destination.query(Funcionario).filter_by(nome = func.nome, func_id = func.func_id).count()
				if total_para_nome_e_func_id == 0:
					# funcionario diferente, mudar func_id e mudar todas as referencias
					# no banco novo para este novo func_id deste funcionario !!!!
					# verificar antes se func_id ja nao esta sendo usado. Procurar
					# valor valido

					destination.add(Funcionario(func.matricula, func.nome,func.dtnasc,
					func.mae, func.pai, func.cpf, func.numrg, func.orgaorg, func.ufrg,
					func.expedrg, func.sit_recag, func.orgao, func.cod_barras, func.st_id,
					func.name, func.words, func.num_vinculos, func.homonimo_status,
					func.estado_civil, func.sexo, func.func_id+1))
					destination.commit()
					max_func_id += 1
					inserts += 1
					func_id_changed += 1
					
				else:
					# existe para o nome e o func_id. Eh o mesmo usuario com matricula diferente!
					registro_novo = destination.query(Funcionario).filter_by(nome = func.nome, func_id = func.func_id).first()
					print '[indexer_'+database_name+'] Mesmo usuario (func_id e nome) com matricula diferente!'
					print '[indexer_'+database_name+'] Func_id: '+str(func.func_id)
					print '[indexer_'+database_name+'] Nome: '+str(func.nome)
					print '[indexer_'+database_name+'] Matricula legada: '+str(func.matricula)
					print '[novo] Matricula nova: '+str(registro_novo.matricula)
					rejecteds += 1

			else:
				rejecteds += 1

		percent = ( ( position * 100 ) / total_records )
		position += 1
		print str(percent) + '%'
		sys.stdout.write("\033[F")

	print "Inseridos: "+str(inserts)
	print "Rejeitados: "+str(rejecteds)
	print "func_id modificado: "+str(func_id_changed)
#	res = source.query(Apostilamento).all()
#	print "###### Apostilamentos:"
#	for apostilamento in res:
#		print apostilamento.nome
#
#	print "###### Funcionarios:"
#	for funcionario in source.query(Funcionario).all():
#		print funcionario.nome
	
databases = ['ambiental', 'auditor', 'bm', 'bm2', 'ceprotec', 'cidadania', 
			'cultura', 'defensor', 'defesa_consumidor', 'des', 'elementar',
			'fazendaria', 'gestor', 'historico', 'indea', 'instrumental',
			'intermat', 'magisterio', 'metrologia', 'militar', 'povo',
			'pm', 'pm2', 'policia_civil', 'procurador', 'regulador', 'seduc',
			'socio_educativo', 'sus', 'taf', 'taf_conferido', 'taf_novo',
			'tecnica', 'transito', 'unemat']

for db in databases:
	execute(db)

#
#	for i in range(10):
#    print("Loading" + "." * i)
#    sys.stdout.write("\033[F") # Cursor up one line
#    time.sleep(1)
#Also sometimes useful (for example if you print something shorter than before):
#
#sys.stdout.write("\033[K") # Clear to the end of line