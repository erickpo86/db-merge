from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class DelimitadorAto(Base):

	__tablename__ = 'delimitador_ato'

	id_ato				= Column(Integer, primary_key=True)
	id_ato_grp			= Column(Integer)
	ato_nome			= Column(String)
	ato_palavra_chave	= Column(String)

	def __init__(self,id_ato, id_ato_grp, ato_nome, ato_palavra_chave):
		self.id_ato				= id_ato
		self.id_ato_grp			= id_ato_grp
		self.ato_nome			= ato_nome
		self.ato_palavra_chave	= ato_palavra_chave
