# -*- coding: utf-8 -*-
from db_connection import get_connection_to
from delimitador_ato import DelimitadorAto
from sqlalchemy.sql import exists
import sys
import logging

def importar_delimitador_ato(database_name):

	source, destination = get_connection_to(database_name)

	delimitadores = source.query(DelimitadorAto).all()

	total_records = source.query(DelimitadorAto).count()

	logging.basicConfig(filename='delimitador_ato.log',level=logging.DEBUG)

	print '\nProcessando '+database_name + ' para Delimitador Ato ['+str(total_records)+']'
	logging.info('Processando '+database_name + ' para Delimitador Ato ['+str(total_records)+']'
	)

	position = 0
	inserts = 0
	rejecteds = 0

	for delimitador in delimitadores:
		""" Importa todos os cargos verificando se existe um cargo com todos os
		dados iguais"""
		existe_delimitador = destination.query(exists().
		where(DelimitadorAto.id_ato == delimitador.id_ato).
		where(DelimitadorAto.id_ato_grp == delimitador.id_ato_grp).
		where(DelimitadorAto.ato_nome == delimitador.ato_nome).
		where(DelimitadorAto.ato_palavra_chave == delimitador.ato_palavra_chave)).scalar()

		if existe_delimitador == False:
			destination.add(DelimitadorAto(delimitador.id_ato, delimitador.id_ato_grp,
							delimitador.ato_nome, delimitador.ato_palavra_chave))
			destination.commit()
			inserts += 1
		else:
#			logging.info('['+database_name+'] Ja existe apostilamento para func_id: '+str(apostilamento.func_id))+' - ignorando registro.'
			rejecteds += 1

		percent = ( ( position * 100 ) / total_records )
		position += 1
		print str(percent) + '%'
		sys.stdout.write("\033[F")

	print "Inseridos: "+str(inserts)
	logging.info('['+database_name+'] Inseridos: '+str(inserts))
	print "Rejeitados: "+str(rejecteds)
	logging.info('['+database_name+'] Rejeitados: '+str(rejecteds))

