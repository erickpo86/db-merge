from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import SmallInteger
from sqlalchemy import Boolean
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class DelimitadorEvento(Base):

	__tablename__ = 'delimitador_evento'

	del_id				= Column(Integer, primary_key=True)
	del_nome			= Column(String)
	del_palavra			= Column(String)
	del_tipo			= Column(Integer)
	del_hierarquia		= Column(SmallInteger)
	ativo				= Column(Boolean)

	def __init__(self, del_nome, del_palavra, del_tipo, del_hierarquia, ativo):
		self.del_nome			= del_nome
		self.del_palavra		= del_palavra
		self.del_tipo			= del_tipo
		self.del_hierarquia		= del_hierarquia
		self.ativo				= ativo
		