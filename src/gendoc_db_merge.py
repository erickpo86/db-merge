from importar_apostilamentos		import importar_apostilamentos
from importar_cargos				import importar_cargos
from importar_delimitador_ato		import importar_delimitador_ato
from importar_delimitador_evento	import importar_delimitador_evento
from importar_funcionarios			import importar_funcionarios
from importar_orgaos				import importar_orgaos

__author__ = "erick"
__date__ = "$27/02/2013 17:15:20$"

databases = ['ambiental', 'auditor', 'bm', 'bm2', 'ceprotec', 'cidadania',
	'cultura', 'defensor', 'defesa_consumidor', 'des', 'elementar',
	'fazendaria', 'gestor', 'historico', 'indea', 'instrumental',
	'intermat', 'magisterio', 'metrologia', 'militar'
	'pm', 'pm2', 'policia_civil', 'procurador', 'regulador', 'seduc',
	'socio_educativo', 'sus', 'taf', 'taf_conferido', 'taf_novo',
	'tecnica', 'transito', 'unemat']

for db in databases:
	importar_funcionarios(db)

for db in databases:
	importar_apostilamentos(db)

for db in databases:
	importar_cargos(db)

for db in databases:
	importar_delimitador_ato(db)

for db in databases:
	importar_delimitador_evento(db)

for db in databases:
	importar_orgaos(db)