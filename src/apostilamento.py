from db_connection import get_connection_to
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
source, destination = get_connection_to('ambiental')

class Apostilamento(Base):

	__tablename__ = 'apostilamento'

	func_id = Column(Integer, primary_key=True)
	nome = Column(String, primary_key=True)

	def __init__(self,func_id, nome):
		self.func_id = func_id
		self.nome = nome