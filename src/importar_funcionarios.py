# -*- coding: iso-8859-15 -*-
from db_connection import get_connection_to
from funcionario import Funcionario
from sqlalchemy.sql import exists
from sqlalchemy import func
import logging
import sys

def importar_funcionarios(database_name):

	source, destination = get_connection_to(database_name)

	funcionarios = source.query(Funcionario).all()

	total_records = source.query(Funcionario).count()

	print '\nProcessando '+database_name + ' para Funcionarios ['+str(total_records)+']'
	logging.info('Processando '+database_name + ' para Funcionarios ['+str(total_records)+']'
	)
	position = 0

	inserts = 0
	rejecteds = 0
	func_id_changed = 0

	for funcionario in funcionarios:

#		total_func_id = destination.query(Funcionario).filter_by(func_id = funcionario.func_id).count()
		exist_func_id = destination.query(exists().where(Funcionario.func_id == funcionario.func_id)).scalar()

#		total_matricula = destination.query(Funcionario).filter_by(matricula = funcionario.matricula).count()
		exist_matricula = destination.query(exists().where(Funcionario.matricula == funcionario.matricula)).scalar()

		if exist_func_id == False:
			# nao existe para este func_id, verificar a matricula e o orgao.
			if exist_matricula == False:
				# nao existe para esta matricula
				destination.add(Funcionario(funcionario.matricula, funcionario.nome,funcionario.dtnasc,
				funcionario.mae, funcionario.pai, funcionario.cpf, funcionario.numrg, funcionario.orgaorg, funcionario.ufrg,
				funcionario.expedrg, funcionario.sit_recag, funcionario.orgao, funcionario.cod_barras, funcionario.st_id,
				funcionario.name, funcionario.words, funcionario.num_vinculos, funcionario.homonimo_status,
				funcionario.estado_civil, funcionario.sexo, funcionario.func_id))
				destination.commit()
				inserts += 1
			else:
				# existe para esta matricula mas nao com o mesmo func_id
				registro_novo = destination.query(Funcionario).filter_by(matricula = funcionario.matricula).first()
				if registro_novo.nome == funcionario.nome:
					#nome igual, matricula igual mas func_id diferente
					print '[indexer_'+database_name+'] Nome igual, matricula igual mas func_id diferente!'
					print '[indexer_'+database_name+'] Nome: '+str(funcionario.name)+' ,Matricula: '+str(funcionario.matricula)+' ,func_id legado: '+str(funcionario.func_id)+' , func_id novo: '+str(registro_novo.func_id)
					rejecteds += 1

				else:
					# nome e func_id diferentes mas matricula igual
					print '[indexer_'+database_name+'] Nome e func_id diferentes mas mesma matricula!'
					print '[indexer_'+database_name+'] Matricula: '+str(funcionario.matricula)
					print '[indexer_'+database_name+'] Nome: '+str(funcionario.name)+' , Func_id: '+str(funcionario.func_id)
					print '[novo] Nome: '+str(registro_novo.name)+' , Func_id: '+str(registro_novo.func_id)
					rejecteds += 1

		else:
			# existe este func_id ja gravado no banco novo
			if exist_matricula == False:
#				total_para_nome_e_func_id = destination.query(Funcionario).filter_by(nome = funcionario.nome, func_id = funcionario.func_id).count()
				existe_nome_e_func_id = destination.query(exists().where(Funcionario.matricula == funcionario.matricula).where(Funcionario.nome==funcionario.nome)).scalar()
				if existe_nome_e_func_id == False:
					# funcionario diferente, mudar func_id e mudar todas as referencias
					# no banco novo para este novo func_id deste funcionario !!!!
					# verificar antes se func_id ja nao esta sendo usado. Procurar
					# valor valido

					novo_func_id = destination.query(func.max(Funcionario.func_id)).scalar()

					destination.add(Funcionario(funcionario.matricula, funcionario.nome,funcionario.dtnasc,
					funcionario.mae, funcionario.pai, funcionario.cpf, funcionario.numrg, funcionario.orgaorg, funcionario.ufrg,
					funcionario.expedrg, funcionario.sit_recag, funcionario.orgao, funcionario.cod_barras, funcionario.st_id,
					funcionario.name, funcionario.words, funcionario.num_vinculos, funcionario.homonimo_status,
					funcionario.estado_civil, funcionario.sexo, novo_func_id+1))
					destination.commit()
					inserts += 1
					func_id_changed += 1

				else:
					# existe para o nome e o func_id. Eh o mesmo usuario com matricula diferente!
					registro_novo = destination.query(Funcionario).filter_by(nome = funcionario.nome, func_id = funcionario.func_id).first()
					print '[indexer_'+database_name+'] Mesmo usuario (func_id e nome) com matricula diferente!'
					print '[indexer_'+database_name+'] Func_id: '+str(funcionario.func_id)
					print '[indexer_'+database_name+'] Nome: '+str(funcionario.nome)
					print '[indexer_'+database_name+'] Matricula legada: '+str(funcionario.matricula)
					print '[novo] Matricula nova: '+str(registro_novo.matricula)
					rejecteds += 1

			else:
				rejecteds += 1

		percent = ( ( position * 100 ) / total_records )
		position += 1
		print str(percent) + '%'
		sys.stdout.write("\033[F")

	print "Inseridos: "+str(inserts)
	print "Rejeitados: "+str(rejecteds)
	print "func_id modificado: "+str(func_id_changed)

