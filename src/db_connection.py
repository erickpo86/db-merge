from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

def	make_session(connection_string):
	engine	=	create_engine(connection_string, echo=False)
	Session	=	sessionmaker(bind=engine)
	return	Session(),	engine

def get_connection_to(db_name):
	source,	sengine	= make_session('postgresql://renato:renato@192.168.1.12/indexer_'+db_name)
	destination, dengine = make_session('postgresql://postgres:postgres@127.0.0.1/gendoc_novo')
	return source, destination

