from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Cargo(Base):

	__tablename__ = 'cargo'

	codigo				= Column(Integer, primary_key=True)
	nome				= Column(String)
	extincao			= Column(String)
	categoria			= Column(String)
	subcategoria		= Column(String)
	adicionado			= Column(Integer)

	def __init__(self,nome, extincao, categoria, subcategoria, adicionado):
		self.nome			= nome
		self.extincao		= extincao
		self.categoria		= categoria
		self.subcategoria	= subcategoria
		self.adicionado		= adicionado
