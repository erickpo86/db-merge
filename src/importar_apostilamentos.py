# -*- coding: utf-8 -*-
from db_connection import get_connection_to
from apostilamento import Apostilamento
from sqlalchemy.sql import exists
import sys
import logging

def importar_apostilamentos(database_name):

	source, destination = get_connection_to(database_name)

	apostilamentos = source.query(Apostilamento).all()

	total_records = source.query(Apostilamento).count()

	logging.basicConfig(filename='apostilamento.log',level=logging.DEBUG)

	print '\nProcessando '+database_name + ' para Apostilamentos ['+str(total_records)+']'
	logging.info('\nProcessando '+database_name + ' para Apostilamentos ['+str(total_records)+']'
	)

	position = 0
	inserts = 0
	rejecteds = 0

	for apostilamento in apostilamentos:
		""" Importa todos os apostilamentos sem verificar as ligacoes com
		funcionarios, somente verificando se nao existe o mesmo apostilamento ja
		inserido"""
		exist_apostilamento = destination.query(exists().where(Apostilamento.func_id == apostilamento.func_id).where(Apostilamento.nome == apostilamento.nome)).scalar()
		
		if exist_apostilamento == False:
			destination.add(Apostilamento(apostilamento.func_id,apostilamento.nome))
			destination.commit()
			inserts += 1
		else:
#			logging.info('['+database_name+'] Ja existe apostilamento para func_id: '+str(apostilamento.func_id))+' - ignorando registro.'
			rejecteds += 1

		percent = ( ( position * 100 ) / total_records )
		position += 1
		print str(percent) + '%'
		sys.stdout.write("\033[F")

	print "Inseridos: "+str(inserts)
	logging.info('['+database_name+'] Inseridos: '+str(inserts))
	print "Rejeitados: "+str(rejecteds)
	logging.info('['+database_name+'] Rejeitados: '+str(rejecteds))

