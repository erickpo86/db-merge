from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Orgao(Base):

	__tablename__ = 'orgao'

	codigo_orgao		= Column(Integer, primary_key=True)
	orgao				= Column(String)
	fantasia			= Column(String)
	sigla				= Column(String)

	def __init__(self, codigo_orgao, orgao, fantasia, sigla):
		self.codigo_orgao	= codigo_orgao
		self.orgao			= orgao
		self.fantasia		= fantasia
		self.sigla			= sigla
