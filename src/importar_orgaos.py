# -*- coding: utf-8 -*-
from db_connection	import get_connection_to
from orgao			import Orgao
from sqlalchemy.sql import exists
from sqlalchemy		import func
import sys
import logging

def importar_orgaos(database_name):

	source, destination = get_connection_to(database_name)

	orgaos = source.query(Orgao).all()

	total_records = source.query(Orgao).count()

	logging.basicConfig(filename='orgao.log',level=logging.DEBUG)

	print '\nProcessando '+database_name + ' para Orgaos ['+str(total_records)+']'
	logging.info('Processando '+database_name + ' para Orgaos ['+str(total_records)+']'
	)

	position = 0
	inserts = 0
	rejecteds = 0

	for orgao in orgaos:
		""" Importa todos os orgaos verificando se existe um orgao com todos os
		dados iguais"""
		existe_orgao = destination.query(exists().where(Orgao.orgao == orgao.orgao).
		where(Orgao.fantasia == orgao.fantasia).
		where(Orgao.codigo_orgao == orgao.codigo_orgao).
		where(Orgao.sigla == orgao.sigla)).scalar()

		if existe_orgao == False:
			if destination.query(exists().where(Orgao.codigo_orgao == orgao.codigo_orgao)).scalar() == True:
				novo_codigo = destination.query(func.max(Orgao.codigo_orgao)).scalar()
				destination.add(Orgao(novo_codigo+1, orgao.orgao, orgao.fantasia, orgao.sigla))
				destination.commit()
				inserts += 1
			else:
				destination.add(Orgao(orgao.codigo_orgao, orgao.orgao, orgao.fantasia, orgao.sigla))
				destination.commit()
				inserts += 1

		else:
#			logging.info('['+database_name+'] Ja existe apostilamento para func_id: '+str(apostilamento.func_id))+' - ignorando registro.'
			rejecteds += 1

		percent = ( ( position * 100 ) / total_records )
		position += 1
		print str(percent) + '%'
		sys.stdout.write("\033[F")

	print "Inseridos: "+str(inserts)
	logging.info('['+database_name+'] Inseridos: '+str(inserts))
	print "Rejeitados: "+str(rejecteds)
	logging.info('['+database_name+'] Rejeitados: '+str(rejecteds))

